#!/bin/sh

INSTALLDIR='/opt/unbouncer'

sudo mkdir -p ${INSTALLDIR}/temp

sudo cp -rf black_lists.cfg \
manual_black_list.cfg \
unbouncer.conf \
white_list.cfg \
white_list_fixed.cfg \
bin \
${INSTALLDIR}

sudo ln -s ${INSTALLDIR}/bin/unbouncer.sh /usr/bin/nsunbouncer

echo "All done, run 'nsunbouncer' to start blocking nasty connections"

